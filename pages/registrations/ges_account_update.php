<?php

session_start();
$id = $_SESSION['id'];
include('db_connection.php');
$rec = mysqli_query($conn, "SELECT * FROM ges_users WHERE id = $id");
$record = mysqli_fetch_array($rec);
$name = $record['name'];
$regional_id = $record['regional_id'];
$user_role = $record['user_role'];
$tel = $record['tel'];
$email = $record['email'];


if($_SERVER["REQUEST_METHOD"] == "POST"){

    if(isset($_POST['name'])){
    $name = $_POST['name'];
}


if(isset($_POST['regional_id'])){
    $regional_id = $_POST['regional_id'];
}

if(isset($_POST['user_role'])){
    $user_role = $_POST['user_role'];
}

if(isset($_POST['tel'])){
    $tel = $_POST['tel'];
}



if(isset($_POST['email'])){
    $email = $_POST['email'];
}


mysqli_query($conn, "UPDATE ges_users SET name = '$name', regional_id = '$regional_id', user_role = '$user_role', tel = '$tel', email = '$email' WHERE id= $id ");

   header('location: ges_user_profile.php');
    echo "Hey";
}


?>


<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>DASHBOARD | GES ADMIN</title>
    <!-- Favicon-->
<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="../../plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="../../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../../css/themes/all-themes.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="SEARCH...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                 <a href="index.php"><img src="ges_logo.jpg" style="border-radius: 100%; height: 100px;"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown" style="padding-top: 25px;">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">2</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu" style="list-style: none;">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">mode_edit</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Ministry of Education</b></h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-orange">
                                                <i class="material-icons">mode_edit</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>The British High Commissioner</b></h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 2 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <!-- Tasks -->
                    <li class="dropdown" style="padding-top: 25px;">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">account_circle</i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="body" style="line-height: 30px;">
                                <ul class="menu tasks" style="list-style: none; padding-top: 10px;">
                          <li><a href="ges_user_profile.php"><i class="fa fa-user fa-fw"></i>User Profile</a>
                        </li>
                        <li><a href="ges_account_update.php"><i class="fa fa-gear fa-fw"></i>Update Account</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-power-off fa-fw"></i>Logout</a>
                        </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Tasks -->
                     <li class="dropdown" style="padding-top: 25px;">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">more_vert</i>
                            
                        </a>
                        <ul class="dropdown-menu" style="border-radius: 10px; width: 60px;">
                            <li class="body" >
                                <p style="text-align: center; font-weight: bold;">Logged On User</p>
                               <p style="text-align: center;"><i class="fa fa-user fa-fw"></i><?php echo $name ?></p>
                                
                               <p style="text-align: center;"><i class="fa fa-envelope fa-fw"></i><?php echo $email ?></p>
                                
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">

            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">
                    <a href="index.php" style="padding-left: 4px;  padding-top: 50px;">
                   GES DASHBOARD
                    </a>
                    </li>
                    <li class="active">
                        <a href="admission_lists.php">
                            <i class="material-icons">view_list</i>
                            <span>ADMISSION</span>
                        </a>
                    </li>
                    <li>
                        <a href="schools.php">
                            <i class="material-icons">school</i>
                            <span>SCHOOLS</span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="material-icons">date_range</i>
                            <span>CALENDER</span>
                        </a>
                    </li>
                     <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">forum</i>
                            <span>COMMUNICATION</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="school_events.php" class="menu-toggle">
                                   <i class="material-icons">event_note</i>
                                    <span>Events for Schools</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">settings</i>
                            <span>SETTINGS</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Empty</span>
                                    <span>Empty</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->

        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
           <!-- <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>-->
            <div class="tab-content">

                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
 
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header" style="padding-top: 40px; margin-left: 31%;">
                <h2>UPDATE YOUR ACCOUNT</h2>
            </div>

                
                       <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card" style="width: 40%; margin-left: 180px; height: 400px;">
                       
                        <div class="body">
                            <div class="table-responsive">

 <form id="sign_up" method="POST" action="">

                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" placeholder="Name" required autofocus value="<?php echo $name ?>">
                        </div>
                    </div>
                   
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">edit</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="regional_id" placeholder="Regional ID" required autofocus value="<?php echo $regional_id ?>">
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">work</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="user_role" placeholder="User Role" required autofocus value="<?php echo $user_role ?>">
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="tel" placeholder="Telephone Number" required autofocus value="<?php echo $tel ?>">
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required value="<?php echo $email ?>">
                        </div>
                    </div>
                    
                    
                    <button  href="index.php" class="btn btn-block btn-lg bg-pink waves-effect" type="submit" id="sign-up" name="sign-up">UPDATE ACCOUNT</button>

                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                
            </div>
            <!-- #END# Widgets -->
            <!-- CPU Usage -->
    </section>

    <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="../../plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="../../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../../plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="../../plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="../../plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="../../plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="../../js/admin.js"></script>
    <script src="../../js/pages/tables/jquery-datatable.js"></script>

</body>

</html>