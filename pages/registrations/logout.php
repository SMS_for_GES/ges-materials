<?php
session_start();
session_destroy();
unset($_SESSION['username']);

echo ("You are now logged out");

header("location: sign-in.php");

?>